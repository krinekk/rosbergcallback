package no.rosberg.callback;

import android.test.ActivityInstrumentationTestCase2;

/**
 * This is a simple framework for a test of an Application.  See
 * {@link android.test.ApplicationTestCase ApplicationTestCase} for more information on
 * how to write and extend Application tests.
 * <p/>
 * To run this test, you can type:
 * adb shell am instrument -w \
 * -e class no.rosberg.callback.DimmerTest \
 * no.rosberg.callback.tests/android.test.InstrumentationTestRunner
 */
public class DimmerTest extends ActivityInstrumentationTestCase2<Dimmer> {

	int a, b;

	public DimmerTest() {
		super("no.rosberg.callback", Dimmer.class);
		a = 1;
		b = 2;
	}

	public void testSampleTest() throws Exception {
		int expected = 3;
		int actual = a + b;
		assertEquals(expected, actual);
	}

}
