package no.rosberg.callback;

import android.content.Context;
import android.content.Intent;
import android.test.ServiceTestCase;
import android.view.View.OnCreateContextMenuListener;

//import com.rosberg.callback.CallbackReceiver;

public class CallbackReceiverTestCase extends ServiceTestCase<CallbackReceiver> {
	
	CallbackReceiver callback = null;
	
	Context context = null;

	public CallbackReceiverTestCase() {
		super(CallbackReceiver.class);
	}
	
	public CallbackReceiverTestCase(Class<CallbackReceiver> activityClass) {
	    super(activityClass);
	    // TODO Auto-generated constructor stub
	}

	protected static void setUpBeforeClass() throws Exception {
	}

	protected static void tearDownAfterClass() throws Exception {
	}

	protected void setUp() throws Exception {
		super.setUp();
	}
	
	void callService(){

        Intent startIntent = new Intent();
        startIntent.setClass(getContext(), CallbackReceiver.class);
        startService(startIntent); 
	}
	
	@Override
	protected void setupService() {
		// TODO Auto-generated method stub
		super.setupService();
        
        callback = getService();
        callback.preloadCallback();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testPreloadCallback() throws Exception {
		Boolean expected = true;
		
        callService();
		
		Boolean actual = callback.preloadCallback();
		
		assertEquals(expected, actual);
	}
	
	@Override
	protected void shutdownService() {
		// TODO Auto-generated method stub
		super.shutdownService();
	}
}
