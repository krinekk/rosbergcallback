package no.rosberg.callback;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;

import android.test.InstrumentationTestCase;

//import com.rosberg.callback.Utils;

public class UtilsTestCase extends InstrumentationTestCase {

	int a;
	int b;
	Utils utils;

	public UtilsTestCase() {
		// TODO Auto-generated constructor stub
	}

	protected void setUp() throws Exception {
		super.setUp();
		this.utils = new Utils();
	}

	protected void tearDown() throws Exception {
		super.tearDown();
	}

	public void testInputStreamAsString() throws Exception {
		
		String expected = "test";
		
		InputStream is = getInstrumentation().getContext().getAssets().open("inputStreamSample.txt");
		String actual = utils.inputStreamAsString(is);
		
		assertEquals(expected, actual);
	}
	
	public void testCharToByte() throws Exception {
		byte[] expected = new byte["test".length()];
		expected = "test".getBytes();
		char char_to_parse[] = "test".toCharArray();
		byte[] actual = utils.charToByte(char_to_parse);
		
		assertEquals(expected.length, actual.length);
		
		for(int i = 0; i < expected.length; ++i){
			assertEquals(expected[i], actual[i]);
		}
	}
	
	public void testWriteBinaryData_noAppend() throws Exception {
		String expected = "test";
		
		String path = getInstrumentation().getContext().getExternalFilesDir(null) + "/testfile";
		
		File file = new File(getInstrumentation().getContext().getExternalFilesDir(null) + "/testfile");
		
		file.deleteOnExit();
		
		Boolean append = false;
		byte[] data = expected.getBytes();
		utils.writeBinaryData(data, file, append);
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(
				new FileInputStream(getInstrumentation().getContext().getExternalFilesDir(null) + "/testfile")));
		String actual = reader.readLine();
		
		reader.close();
		
		assertEquals(expected, actual);
	}
	
	public void testWriteBinaryData_Append() throws Exception {
		String expected = "test";
		
		String path = getInstrumentation().getContext().getExternalFilesDir(null) + "/testfile";
		
		File file = new File(getInstrumentation().getContext().getExternalFilesDir(null) + "/testfile");
		
		file.deleteOnExit();
		
		Boolean append = false;
		byte[] data = expected.getBytes();
		utils.writeBinaryData(data, file, append);
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(
				new FileInputStream(getInstrumentation().getContext().getExternalFilesDir(null) + "/testfile")));
		String actual = reader.readLine();
		
		reader.close();
		
		assertEquals(expected, actual);
		
		append = true;
		
		utils.writeBinaryData(data, file, append);
		
		reader = new BufferedReader(new InputStreamReader(
				new FileInputStream(getInstrumentation().getContext().getExternalFilesDir(null) + "/testfile")));
		actual = reader.readLine();
		
		reader.close();
		
		assertEquals(expected + expected, actual);
		
	}

}
