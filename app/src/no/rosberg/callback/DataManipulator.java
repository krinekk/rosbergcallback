package no.rosberg.callback;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;

import net.sqlcipher.database.SQLiteDatabase;
import net.sqlcipher.database.SQLiteException;
import net.sqlcipher.database.SQLiteOpenHelper;
import net.sqlcipher.database.SQLiteStatement;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.database.SQLException;
import android.util.Log;

public class DataManipulator extends SQLiteOpenHelper{

	//The Android's default system path of your application database.
	String DB_PATH =null;

	private static String DB_NAME = "TPMdb.db";

	private static String DB_PASSWORD = "rosberg";

	public static String TBL_CALLBACK = "tblCallback";

	private SQLiteDatabase myDataBase; 

	private final Context myContext;

	Utils utils;

	private Cursor cursor;

	/**
	 * Constructor
	 * Takes and keeps a reference of the passed context in order to access to the application assets and resources.
	 * @param context
	 */
	public DataManipulator(Context context) {

		super(context, DB_NAME, null, 1);

		this.utils = new Utils();
		this.myContext = context;

		try{
			SQLiteDatabase.loadLibs(myContext);			
		}catch(NullPointerException e){
			utils.logException(e); 
		}

		DB_PATH= utils.APP_DIR + "/databases/";
	}   

	/**
	 * Creates a empty database on the system and rewrites it with your own database.
	 * @throws NameNotFoundException 
	 * */
	public Boolean createDataBase(Context context) throws IOException, NameNotFoundException{

		boolean dbExist = checkDataBase(context);
		return dbExist;
	}

	/**
	 * Creates a empty database on the system and rewrites it with your own database.
	 * */
	public void unlockDatabase() throws IOException{
		myDataBase = this.getWritableDatabase(DB_PASSWORD);
	}
	
	public Boolean isOpen(){
		return myDataBase.isOpen();
	}

	public void deleteDatabaseRows(){
		myDataBase.delete(TBL_CALLBACK, null, null);
	}

	/**
	 * Check if the database already exist to avoid re-copying the file each time you open the application.
	 * @return true if it exists, false if it doesn't
	 * @throws IOException 
	 * @throws NameNotFoundException 
	 */
	private boolean checkDataBase(Context context) throws NameNotFoundException, IOException{

		SQLiteDatabase.loadLibs(context);	

		try{
			File databaseFile = myContext.getDatabasePath(DB_NAME);
			if(!databaseFile.exists()){
				databaseFile.mkdirs();
				databaseFile.delete();
				databaseFile.createNewFile();
			}
			myDataBase = SQLiteDatabase.openOrCreateDatabase(databaseFile
					, DB_PASSWORD, null);

		}catch(SQLiteException e){

			utils.log("Rosberg Callback", this, "Database doesn't exists.", Log.INFO);

		}

		if(myDataBase != null){
			try {
				cursor = myDataBase.query(TBL_CALLBACK, null, null, null, null, null, null);

				Boolean hasrows = cursor.moveToFirst();

				cursor.close();
				if(!hasrows) return false;

				myDataBase.close();
			} catch (SQLiteException e) {
				if(cursor != null)
					cursor.close();

				utils.log("Rosberg Callback", this, e.getMessage(), Log.INFO);

				myDataBase.
				execSQL("CREATE TABLE tblCallback " +
						"(id_caller TEXT PRIMARY KEY,action TEXT,parameter TEXT,signature BINARY)");
				myDataBase.close();
				return false;
			}

		}

		return myDataBase != null ? true : false;
	}

	/**
	 * Copies your database from your local assets-folder to the just created empty database in the
	 * system folder, from where it can be accessed and handled.
	 * This is done by transfering bytestream.
	 * */
	private void copyDataBase() throws IOException{

		/*
		//Open your local db as the input stream
		InputStream myInput = myContext.getAssets().open(DB_NAME);

		// Path to the just created empty db
		String outFileName = DB_PATH + DB_NAME;

		//Open the empty db as the output stream
		OutputStream myOutput = new FileOutputStream(outFileName);

		//transfer bytes from the inputfile to the outputfile
		byte[] buffer = new byte[1024];
		int length;
		while ((length = myInput.read(buffer))>0){
			myOutput.write(buffer, 0, length);
		}

		//Close the streams
		myOutput.flush();
		myOutput.close();
		myInput.close();
		 */
		myDataBase.execSQL("CREATE TABLE tblCallback (id_caller TEXT PRIMARY KEY,action TEXT,parameter TEXT)");
	}

	public void openDataBase() throws SQLException, IOException, NameNotFoundException{

		try{
			File databaseFile = myContext.getDatabasePath(DB_NAME);
			if(!databaseFile.exists()){
				databaseFile.mkdirs();
				databaseFile.delete();
				databaseFile.createNewFile();
			}
			myDataBase = SQLiteDatabase.openOrCreateDatabase(databaseFile
					, DB_PASSWORD, null);

		}catch(SQLiteException e){

			utils.log("Rosberg Callback", this, "Database doesn't exists.", Log.INFO);

		}
	}

	@Override
	public synchronized void close() {

		if(myDataBase != null)
			myDataBase.close();

		super.close();

	}

	@Override
	public void onCreate(SQLiteDatabase db) {

	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

	}
	//return cursor
	public Cursor query(String table,String[] columns, String selection,String[] selectionArgs,String groupBy,String having,String orderBy){
		return myDataBase.query(table, columns, selection, selectionArgs, groupBy, having, orderBy);
	}
	public Cursor query(String table, String column, String value){
		return myDataBase.rawQuery("SELECT * FROM " + table + 
				" WHERE " + column + " = '"+value+"'", null);
	}
	public Cursor query(String table){
		return myDataBase.rawQuery("SELECT * FROM " + table, null);
	}
	public void insert(String id_caller, String action, String parameter){
		ContentValues values = new ContentValues();
		values.put("id_caller", id_caller);
		values.put("action", action);
		values.put("parameter", parameter);
		myDataBase.insert(TBL_CALLBACK, null, values);
	}

	public void insertData(JSONArray json) {
		Date startDate = new Date();

		String sql = "INSERT INTO " + TBL_CALLBACK + " (id_caller, action, parameter, signature) VALUES (?, ?, ?, ?)";
		myDataBase.beginTransaction();

		SQLiteStatement stmt = myDataBase.compileStatement(sql);
		for (int i = 0; i < json.length(); i++) {
			JSONObject tmp;
			try {
				tmp = json.getJSONObject(i);
				stmt.bindString(1, tmp.getString("id_caller"));
				stmt.bindString(2, tmp.getString("action"));
				stmt.bindString(3, tmp.getString("parameter"));
				stmt.bindString(4, tmp.getString("signature"));
				stmt.execute();
				stmt.clearBindings();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				utils.logException(e);
			}
		}

		myDataBase.setTransactionSuccessful();
		myDataBase.endTransaction();

		try {
			utils.log("Database Insertion", this, json.length() + " rows inserted in "
					+ ((new Date()).getTime()/1000 - startDate.getTime()/1000) + " seconds." , Log.INFO);
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			utils.logException(e);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			utils.logException(e);
		}
	}
}
