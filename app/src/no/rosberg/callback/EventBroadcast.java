package no.rosberg.callback;

import java.io.IOException;
import java.util.Locale;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.CallLog;
import android.telephony.SmsMessage;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;

/**
 * Class used to recieve different events from device.
 * @author krinekk
 *
 */
public class EventBroadcast extends BroadcastReceiver
{

	@Override
	public void onReceive(Context context, Intent intent) 
	{	
		Utils utils = new Utils();
		/*

		 */
		
		String action = intent.getAction();

		/**
		 * Receiver containing the connectivity status.
		 */
		if(intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {

			try {
				utils.log("BroadcastReciever", this, "BOOT_COMPLETED Recieved", Log.INFO);
			} catch (NameNotFoundException e) {
				// TODO Auto-generated catch block
				utils.logException(e);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				utils.logException(e);
			}
			Intent msgIntent = new Intent(context, CallbackReceiver.class);

			msgIntent.putExtra(CallbackReceiver.NUMBER_RECIEVED, "");
			context.startService(msgIntent);
		}else if (intent.getAction().equals("android.intent.action.PHONE_STATE") && intent.getStringExtra(TelephonyManager.EXTRA_STATE).equals(
				TelephonyManager.EXTRA_STATE_IDLE)) {

			try {
				utils.log("BroadcastReciever", this, "Call received.", Log.INFO);
			} catch (NameNotFoundException e) {
				// TODO Auto-generated catch block
				utils.logException(e);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				utils.logException(e);
			}

			synchronized(this){
				try {
					this.wait(500);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					utils.logException(e);
				}
			}

			String[] columns = { CallLog.Calls.NUMBER, CallLog.Calls.TYPE, CallLog.Calls.IS_READ};
			Cursor c = context.getContentResolver().query(CallLog.Calls.CONTENT_URI,columns,null, null, null);

			String caller_id = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);
			
			try {
				utils.log("BroadcastReciever", this, "Caller Id: " + caller_id, Log.INFO);
			} catch (NameNotFoundException e) {
				// TODO Auto-generated catch block
				utils.logException(e);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				utils.logException(e);
			}
			
			c.moveToFirst();

			String number = c.getString(0);
			int type = c.getInt(1);
			int is_read = c.getInt(2);

			if(caller_id != null && caller_id.equals(number) && type == CallLog.Calls.MISSED_TYPE && is_read == 0){
				TelephonyManager manager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
				String deviceCountry = manager.getNetworkCountryIso().toUpperCase(Locale.getDefault());

				PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();

				PhoneNumber caller_number = null;

				try {
					caller_number = phoneUtil.parse(caller_id, deviceCountry);
					String s  = "";
				} catch (NumberParseException e) {
					try {
						utils.log("Rosberg Callback", this, "NumberParseException was thrown: " + e.toString(), Log.ERROR);
					} catch (NameNotFoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}

				caller_id = phoneUtil.format(caller_number, PhoneNumberFormat.E164);

				try {
					utils.log("BroadcastReciever", this, "Caller Id: " + caller_id, Log.INFO);
				} catch (NameNotFoundException e) {
					// TODO Auto-generated catch block
					utils.logException(e);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					utils.logException(e);
				}	

				Intent msgIntent = new Intent(context, CallbackReceiver.class);

				msgIntent.putExtra(CallbackReceiver.NUMBER_RECIEVED, caller_id);
				context.startService(msgIntent);
			}	
			c.close();
		}else if(intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")){
			String caller_id = readSMS(context, intent);
			if(caller_id != null){
				Intent msgIntent = new Intent(context, CallbackReceiver.class);
				//msgIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				msgIntent.putExtra(CallbackReceiver.NUMBER_RECIEVED, caller_id);
				context.startService(msgIntent);
			}
		}else if (intent.getAction().equals(DownloadManager.ACTION_DOWNLOAD_COMPLETE)) {


			long downloadId = intent.getLongExtra(
					DownloadManager.EXTRA_DOWNLOAD_ID, 0);

			try {
				utils.log("BroadcastReciever", this, "ACTION_DOWNLOAD_COMPLETE Recieved", Log.INFO);
			} catch (NameNotFoundException e) {
				// TODO Auto-generated catch block
				utils.logException(e);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				utils.logException(e);
			}
			Intent msgIntent = new Intent(context, CallbackReceiver.class);

			msgIntent.putExtra(CallbackReceiver.DOWNLOAD_COMPLETE, downloadId);
			context.startService(msgIntent);
		}
	}	

	/**
	 * Read the incoming SMS to get the caller id.
	 * @param context The application context
	 * @param intent The intent of application that get that message
	 * @return A Sting that contains the caller id.
	 */
	private String readSMS(Context context, Intent intent) {
		//---get the SMS message passed in---
		Bundle bundle = intent.getExtras();
		SmsMessage[] msgs = null;
		String str = "";
		if (bundle != null)
		{
			//---retrieve the SMS message received---
			Object[] pdus = (Object[]) bundle.get("pdus");
			msgs = new SmsMessage[pdus.length];
			msgs[0] = SmsMessage.createFromPdu((byte[])pdus[0]);
			/*
			str += "SMS from " + msgs[0].getOriginatingAddress();
			str += " :";
			str += msgs[0].getMessageBody().toString();
			str += "\n";
			//---display the new SMS message---
			Toast.makeText(context, str, Toast.LENGTH_SHORT).show();
			 */
			return msgs[0].getOriginatingAddress();
		}
		return null;
	}
}
