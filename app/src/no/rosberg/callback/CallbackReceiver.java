package no.rosberg.callback;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.spec.KeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.RSAPrivateCrtKeySpec;
import java.util.List;

import javax.net.ssl.SSLPeerUnverifiedException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;

import android.app.DownloadManager;
import android.app.DownloadManager.Query;
import android.app.IntentService;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.database.SQLException;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

import no.rosberg.callback.Utils.ShellCommand;

public class CallbackReceiver extends IntentService {

	Utils utils;

	DataManipulator myDB;

	/**
	 * Message identifier to advice that device is booted.
	 */
	public final static String DEVICE_BOOTED = "no.rosberg.callback.deviceBooted";

	public static final String NUMBER_RECIEVED = "no.rosberg.callback.numberReceived";

	public static final String DOWNLOAD_COMPLETE = "no.rosberg.callback.downloadComplete";

	public static final String CB_ADMIN = "ADMIN";
	public static final String CB_VPN = "VPN";
	public static final String CB_DOCUMENT = "DOCUMENT";
	public static final String CB_INVENTORY = "INVENTORY";
	public static final String CB_URL = "URL";

	public static final String CALLBACK_CONFIG_URL = "https://medialabtestserver.tk/tblCallback.php";

	private static final int NUM_TRIES = 5;

	public String vpn_profile = "";

	public String caller_id = "";

	Cursor cursor;

	DownloadManager manager;

	// declare the dialog as a member field of your activity
	ProgressDialog mProgressDialog;

	private Handler mHandler;

	public CallbackReceiver() {
		super("CallbackReciever");
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate() {
		super.onCreate();
		mHandler = new Handler();
	}

	@Override
	public void onStart(Intent intent, int startId) {
		// TODO Auto-generated method stub
		super.onStart(intent, startId);
	}

	@Override
	public void onDestroy() {
		if(mHandler != null)
			mHandler = null;
		super.onDestroy();
		String s = "";
	}

	/*	
	@Override
	public void onStart(Intent intent, int startId) {
		// TODO Auto-generated method stub
		try {
			utils.log("Rosberg Callback", this, "Callback Service started", Log.INFO);
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		super.onStart(intent, startId);
	}
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub
		return START_STICKY;
	}
	 */

	public Boolean preloadCallback(){


		utils = new Utils();	

		myDB = new DataManipulator(this);

		Boolean hasDB = false;

		try {

			hasDB = myDB.createDataBase(this);

		} catch (Error e) {

			try {
				utils.log("Rosberg DB", this, "Unable to create database", Log.ERROR);
			} catch (NameNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				return false;
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				return false;
			}

		} catch (IOException ioe) {

			try {
				utils.log("Rosberg DB", this, "Unable to create database", Log.ERROR);
			} catch (NameNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				return false;
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				return false;
			}

			if(myDB.isOpen())
				myDB.close();

		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			utils.logException(e);
			return false;
		}

		if(!hasDB){
			try {
				downloadNewDB();
			} catch (SSLPeerUnverifiedException e) {
				try {
					utils.log("Rosberg Callback", this, "Certificate error: " + e.getMessage(), Log.ERROR);
				} catch (NameNotFoundException e1) {
					// TODO Auto-generated catch block
					utils.logException(e1);
					return false;
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					utils.logException(e1);
					return false;
				}
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				utils.logException(e);
				return false;
			} catch (NameNotFoundException e) {
				// TODO Auto-generated catch block
				utils.logException(e);
				return false;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				utils.logException(e);
				return false;
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				utils.logException(e);
				return false;
			}
		}

		try {
			createAdditionalFiles();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			utils.logException(e);
			return false;
		}
		return true;	
	}

	@Override
	protected void onHandleIntent(Intent intent) {

		Boolean preload_ok = preloadCallback();

		Bundle extras = intent.getExtras();

		if(extras != null){

			if(extras.containsKey(NUMBER_RECIEVED)){
				caller_id = intent.getExtras().getString(NUMBER_RECIEVED);


				Boolean isInDB = checkCaller(caller_id);
				String action = checkAction(caller_id);

				if(isInDB){
					if(isRowTrusted(caller_id)){
						if(action.equals(CB_ADMIN)){
							try {
								downloadNewDB();
							} catch (ClientProtocolException e) {
								// TODO Auto-generated catch block
								utils.logException(e);
							} catch (SSLPeerUnverifiedException e2) {
								try {
									utils.log("Rosberg Callback", this, "Certificate error: " + e2.getMessage(), Log.ERROR);
								} catch (NameNotFoundException e) {
									// TODO Auto-generated catch block
									utils.logException(e);
								} catch (IOException e) {
									// TODO Auto-generated catch block
									utils.logException(e);
								}
							} catch (IOException e) {
								// TODO Auto-generated catch block
								utils.logException(e);
							} catch (NameNotFoundException e) {
								// TODO Auto-generated catch block
								utils.logException(e);
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								utils.logException(e);
							}
						}else if(action.equals(CB_VPN)){
							String vpn_gateway = getParameter();
							try {
								Boolean is_started = false;
								int i = 0;
								while(!is_started && i < NUM_TRIES){
									is_started = StartOpenVPNConnection(vpn_gateway);
									++i;
								}
							} catch (NameNotFoundException e) {
								// TODO Auto-generated catch block
								utils.logException(e);
							} catch (IOException e) {
								// TODO Auto-generated catch block
								utils.logException(e);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								utils.logException(e);
							}
						}else if(action.equals(CB_DOCUMENT)){
							getDocument();
						}else if(action.equals(CB_URL)){
							String parameter = getParameter();
							openWebBrowser(parameter);
						}else if(action.equals(CB_INVENTORY)){
							String parameter = getParameter();
							Intent i = new Intent("rosberg.intent.action.LOAD_INVENTORY");
							i.putExtra("no.rosberg.inventory.url", parameter);
							sendOrderedBroadcast(i, null);
						}
					}else{
						try {
							utils.log("Rosberg Callback", this, "Security Exception: Row is untrusted.", Log.ERROR);
						} catch (NameNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			}else if(extras.containsKey(DOWNLOAD_COMPLETE)){
				long download_id = intent.getExtras().getLong(DOWNLOAD_COMPLETE);
				openDocument(download_id);
			}
		}
		if(cursor != null)
			cursor.close();
	}

	public void downloadNewDB() throws ClientProtocolException, IOException, NameNotFoundException, JSONException {
		utils.log("Rosberg Callback", this, "Starting a SSL connection...", Log.INFO);
		// Instantiate the custom HttpClient
		DefaultHttpClient client = new HttpsClient(getApplicationContext());
		HttpGet get = new HttpGet(CALLBACK_CONFIG_URL);
		//HttpGet get = new HttpGet("https://172.16.14.227/tblcallback_objects.js");
		// Execute the GET call and obtain the response
		HttpResponse getResponse = client.execute(get);
		HttpEntity responseEntity = getResponse.getEntity();

		utils.log("Rosberg Callback", this, "Peer certificates are correct, getting content...", Log.INFO);

		InputStream in = responseEntity.getContent();

		utils.log("Rosberg Callback", this, "Updating Database...", Log.INFO);

		String jsonString = Utils.inputStreamAsString(in);

		myDB.unlockDatabase();

		JSONArray json = new JSONArray(jsonString.replace("<pre>", "").replace("</pre>", ""));

		/*
		myDB.deleteDatabaseRows();

		Date startDate = new Date();


		for(int i = 0; i < json.length(); ++i){
			JSONObject tmp = json.getJSONObject(i);
			String id_caller = tmp.getString("id_caller");
			String action = tmp.getString("action");
			String parameter = tmp.getString("parameter");
			myDB.insert(id_caller, action, parameter);
		}


		utils.log("Simple Insertion", this, json.length() + " rows inserted in "
		+ ((new Date()).getTime()/1000 - startDate.getTime()/1000) + " seconds." , Log.INFO);

		 */
		myDB.deleteDatabaseRows();

		myDB.insertData(json);

		utils.log("Rosberg Callback", this, "Database Updated...", Log.INFO);

		if(myDB.isOpen())
			myDB.close();
	}

	private void openWebBrowser(String url) {
		if (!url.startsWith("http://") && !url.startsWith("https://"))
		{
			url = "http://" + url;
		}

		Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
		//startActivity(Intent.createChooser(intent, "Choose browser").addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
	}

	private void getDocument() { 
		String url = getParameter();
		String path = Environment.getExternalStorageDirectory().getPath()
				+ "/" + Environment.DIRECTORY_DOWNLOADS
				+ "/" + url.substring(url.lastIndexOf("/")+1);
		File file = new File(Environment.getExternalStorageDirectory().getPath()
				+ "/" + Environment.DIRECTORY_DOWNLOADS
				+ "/" + url.substring(url.lastIndexOf("/")+1));
		if(file.exists()){
			openDocument(file);
		}else{
			downloadFile(url);
		}
	}

	private void downloadFile(String url) {
		// execute this when the downloader must be fired
		/*DownloadFile downloadFile = new DownloadFile();
		downloadFile.execute(url);*/
		DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
		request.setDescription(url);
		request.setTitle(url.substring(url.lastIndexOf("/")+1));
		// in order for this if to run, you must use the android 3.2 to compile your app
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			request.allowScanningByMediaScanner();
			request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE);
		}
		request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, url.substring(url.lastIndexOf("/")+1));

		// get download service and enqueue file
		manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
		manager.enqueue(request);

		try {
			utils.log("Rosberg Callback", this, "Download Started.", Log.INFO);
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			utils.logException(e);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			utils.logException(e);
		}
	}

	private void openDocument(File file){
		Intent intent = new Intent();
		intent.setAction(android.content.Intent.ACTION_VIEW);

		String mimeTypeMap =MimeTypeMap.getFileExtensionFromUrl(file.getName());
		String mime_type = MimeTypeMap.getSingleton()
				.getMimeTypeFromExtension(mimeTypeMap);

		List list = getPackageManager().queryIntentActivities(intent,0);

		if (list.size() > 0) {
			intent.setDataAndType(Uri.fromFile(file), mime_type);
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent); 
		}else{
			mHandler.post(new DisplayToast("Content not supported."));
		}
	}

	private void openDocument(long download_id){

		manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);

		Query query = new Query();
		query.setFilterById(download_id);
		Cursor c = manager.query(query);
		if (c.moveToFirst()) {
			int columnIndex = c
					.getColumnIndex(DownloadManager.COLUMN_STATUS);
			if (DownloadManager.STATUS_SUCCESSFUL == c
					.getInt(columnIndex)) {

				String uriString = c
						.getString(c
								.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI));
				Uri uri = Uri.parse(uriString);

				Intent intent = new Intent();
				intent.setAction(Intent.ACTION_VIEW);		

				String mimeTypeMap =MimeTypeMap.getFileExtensionFromUrl(uriString);
				String mime_type = MimeTypeMap.getSingleton()
						.getMimeTypeFromExtension(mimeTypeMap);

				try {
					utils.log("Rosberg Callback", this, "Document type: " + mime_type, Log.INFO);
				} catch (NameNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				List list = getPackageManager().queryIntentActivities(intent,0);


				if (list.size() > 0) {
					intent.setDataAndType(uri, mime_type);
					intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(intent); 
				}else{
					mHandler.post(new DisplayToast("Content not supported."));
				}
			}
		}


		/*
		Intent intent = new Intent();
		intent.setAction(android.content.Intent.ACTION_VIEW);
		File file = new File(file_path);

		String mimeTypeMap =MimeTypeMap.getFileExtensionFromUrl(file_path);
		String mime_type = MimeTypeMap.getSingleton()
		                        .getMimeTypeFromExtension(mimeTypeMap);

		intent.setDataAndType(Uri.fromFile(file), mime_type);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent); 
		 */
		/*
		Intent intent = new Intent(Intent.ACTION_VIEW,
		        Uri.parse("/sdcard/Download/Adobe Reader/12013025166.pdf"));
		//intent.setType("application/pdf");
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	    startActivity(intent);
		 */

		/*
		PackageManager pm = getPackageManager();
		List<ResolveInfo> activities = pm.queryIntentActivities(intent, 0);
		if (activities.size() > 0) {
		    startActivity(intent);
		} else {
			Toast.makeText(getApplicationContext(), "Not any pdf reader installed.", Toast.LENGTH_SHORT).show();
		}*/
	}

	PublicKey readPublicKey() throws Exception {
		InputStream in = getResources().openRawResource(R.raw.callback_config_public);
		byte[] buff = new byte[4000];
		int bytesRead;
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		while((bytesRead = in.read(buff)) != -1) {
			out.write(buff, 0, bytesRead);
			//Log.i(TAG, "bytes read: " + bytesRead);
		}

		byte[] publicKeyBytes = out.toByteArray();

		CertificateFactory cf = CertificateFactory.getInstance("X509");
		Certificate cert = cf.generateCertificate(new ByteArrayInputStream(publicKeyBytes));

		PublicKey pubKey = cert.getPublicKey();
		return pubKey;
	}

	private Boolean isRowTrusted(String id) {

		Boolean trusted = false;

		try {

			myDB.openDataBase();

			cursor = myDB.query(DataManipulator.TBL_CALLBACK, "id_caller", id);
			if(cursor.getCount() > 0){

				cursor.moveToFirst();
				PublicKey public_key = null;

				String signature = cursor.getString(cursor.getColumnIndex("signature"));

				String data = cursor.getString(cursor.getColumnIndex("id_caller"))
						+ cursor.getString(cursor.getColumnIndex("action"))
						+ cursor.getString(cursor.getColumnIndex("parameter"));

				public_key = readPublicKey();
				Signature instance = Signature.getInstance("SHA1withRSA");
				instance.initVerify(public_key);
				instance.update((data).getBytes());
				trusted = instance.verify(Base64.decode(signature, Base64.DEFAULT));
			}
		} catch (SignatureException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return trusted;
	}

	public Boolean checkCaller(String id) {
		if(id != null){ 

			try {

				myDB.openDataBase();
				cursor = myDB.query(DataManipulator.TBL_CALLBACK, "id_caller", id);
				if(cursor.getCount() > 0){

					try {
						utils.log("Rosberg Callback", this, "Incoming number is a valid callback number", Log.INFO);
					} catch (NameNotFoundException e) {
						// TODO Auto-generated catch block
						utils.logException(e);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						utils.logException(e);
					}
					cursor.close();
					if(myDB.isOpen())
						myDB.close();
					return true;
				}else{
					cursor.close();
					if(myDB.isOpen())
						myDB.close();
					return false;
				}
			} catch (IOException ioe) {

				throw new Error("Unable to create database");

			}catch(SQLException sqle){

				throw sqle;

			} catch (NameNotFoundException e) {
				// TODO Auto-generated catch block
				utils.logException(e);
			} catch (IndexOutOfBoundsException e) {
				// TODO Auto-generated catch block
				utils.logException(e);
			}
		}
		cursor.close();
		if(myDB.isOpen())
			myDB.close();

		return false;
	}

	private String checkAction(String id) {

		if(id != null){

			String action = null;

			try {

				myDB.openDataBase();

				cursor = myDB.query(DataManipulator.TBL_CALLBACK, "id_caller", id);

				Boolean hasrows = cursor.moveToFirst();

				if(hasrows){
					action  = cursor.getString(cursor.getColumnIndex("action"));	
				}else{
					return action;
				}

				utils.log("Rosberg Callback", this, "Callback mode: " + action, Log.INFO);

			} catch (IOException ioe) {

				throw new Error("Unable to create database");

			}catch(SQLException sqle){

				throw sqle;

			} catch (NameNotFoundException e) {
				// TODO Auto-generated catch block
				utils.logException(e);
			} catch (IndexOutOfBoundsException e) {
				// TODO Auto-generated catch block
				utils.logException(e);
			}

			if(!cursor.isClosed())
				cursor.close();
			if(myDB.isOpen())
				myDB.close();

			return action;
		}

		if(!cursor.isClosed())
			cursor.close();
		if(myDB.isOpen())
			myDB.close();

		return null;
	}

	private String getParameter() {
		// TODO Auto-generated method stub
		cursor.moveToFirst();
		return cursor.getString(cursor.getColumnIndex("parameter"));
	}

	public boolean StartOpenVPNConnection(String vpn_gateway) throws NameNotFoundException, IOException, InterruptedException{
		/*
		final String EXTRA_NAME = "de.blinkt.openvpn.shortcutProfileName";
		final String EXTRA_HIDELOG =  "de.blinkt.openvpn.showNoLogWindow";

		Intent shortcutIntent = new Intent(Intent.ACTION_MAIN);
		shortcutIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		shortcutIntent.setClassName("de.blinkt.openvpn", "de.blinkt.openvpn.LaunchVPN");
		shortcutIntent.putExtra(EXTRA_NAME,profile_name);
		shortcutIntent.putExtra(EXTRA_HIDELOG,true);
		startActivity(shortcutIntent);
		 */
		if(vpn_gateway != null && vpn_gateway != ""){

			utils.log("Rosberg Callback", this, "Enabling Callback VPN", Log.INFO);

			String run_command = "openvpn --remote " + vpn_gateway + 
					" --config " + utils.APP_DIR + "/files/openvpn/android_vpn.conf";

			ShellCommand cmd = (new Utils()).new ShellCommand();
			cmd.suOrSH().run("killall -9 openvpn");

			utils.log("Rosberg Callback", this, "Checking Callback VPN Previous Instance", Log.INFO);

			synchronized(this){
				this.wait(2000);
			}

			cmd.suOrSH().run(run_command);

			utils.log("Rosberg Callback", this, "Callback VPN Established", Log.INFO);

			return true;
			/*
			if (!r.success()) {
				utils.log("XMLRPCServerService", this, "Callback Error: " + r.stderr, Log.ERROR);
				return false;
			} else {
				utils.log("XMLRPCServerService", this, "Callback Success: " + r.stdout, Log.INFO);
				return true;
			}
			 */
		}else 
			return false;
	}

	public void createAdditionalFiles() throws IOException {
		// TODO Auto-generated method stub
		copyFile("openvpn/ca.crt");
		copyFile("openvpn/android_vpn.conf");
		copyFile("openvpn/android.crt");
		copyFile("openvpn/android.key");
	}

	private void copyFile(String asset) throws IOException {
		// TODO Auto-generated method stub

		String outFileName = utils.APP_DIR + "/files/" + asset;
		File file = new File(outFileName); 
		file.getParentFile().mkdirs();
		file.createNewFile();
		InputStream myInput = this.getAssets().open(asset);

		//Open the empty db as the output stream
		OutputStream myOutput = new FileOutputStream(outFileName);

		//transfer bytes from the inputfile to the outputfile
		byte[] buffer = new byte[1024];
		int length;
		while ((length = myInput.read(buffer))>0){
			myOutput.write(buffer, 0, length);
		}

		//Close the streams
		myOutput.flush();
		myOutput.close();
		myInput.close();
	}

	private class DisplayToast implements Runnable{
		String mText;

		public DisplayToast(String text){
			mText = text;
		}

		public void run(){
			Toast.makeText(getApplicationContext(), mText, Toast.LENGTH_SHORT).show();
		}
	}

}
