package no.rosberg.callback;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
/**
 * Class container of utilities.
 * @author krinekk
 *
 */
public class Utils {

	/**
	 * Application directory.
	 */
	public final String APP_DIR = "/data/data/no.rosberg.callback/";
	/**
	 * Log name.
	 */
	private static final String LOGFILE = "/callback.log";
	/**
	 * Maximum number of logs to be saved on the device.
	 */
	public final int MAX_LOG_NUM = 8;
	/**
	 * Maximum number of inventories to be saved on the device.
	 */
	public final int MAX_INVENTORIES = 5;
	/**
	 * Maximum size of logs.
	 */
	public final int LOGSIZE = 522240;

	/**
	 * Converts inputStream to String.
	 * @param stream The input stream to convert.
	 * @return The string parsed.
	 */
	public static String inputStreamAsString(InputStream stream)
			throws IOException {

		BufferedReader br = new BufferedReader(new InputStreamReader(stream,
				"UTF-8"));
		StringBuilder sb = new StringBuilder();

		int cp;
	    while ((cp = br.read()) != -1) {
	      sb.append((char) cp);
	    }

		br.close();

		return sb.toString();
	}
	/**
	 * Insert an log entry to custom logger. 
	 * @param handler The service handler that sends a log
	 * @param obj The object that sends a log.
	 * @param msg Message to display.
	 * @param level Log level. Defined by native Log().
	 * @throws NameNotFoundException
	 * @throws IOException
	 */
	public void log(String handler, Object obj, String msg, int level) throws NameNotFoundException, IOException {

		SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd HH:mm:ss.SSS", Locale.getDefault());
		String date = dateFormat.format(new Date());

		String lvl;

		switch (level) {
		case Log.ASSERT:
			lvl = "A/";
			break;
		case Log.DEBUG:
			lvl = "D/";
			break;
		case Log.ERROR:
			lvl = "E/";
			break;
		case Log.INFO:
			lvl = "I/";
			break;
		case Log.VERBOSE:
			lvl = "V/";
			break;
		case Log.WARN:
			lvl = "W/";
			break;
		default:
			lvl = "";
			break;
		}

		String final_msg = String.format("[%s] %s", obj.getClass().getName(),
				msg);
		Log.println(level, handler, final_msg);


		final_msg = String.format("%s: %s%s(%d): [%s] %s\n", 
				date, 
				lvl,
				handler,
				android.os.Process.myPid(),
				obj.getClass().getName(),
				msg);

		File logfile, pathlog;

		pathlog = new File(APP_DIR);

		logfile = new File(APP_DIR + LOGFILE);

		if(logfile.length() + final_msg.length() >= LOGSIZE){
			moveLine(logfile, getNextLog(logfile), final_msg);
		}else{
			FileOutputStream fop = null;
			fop = new FileOutputStream(logfile, true);
			fop.write(final_msg.getBytes());
			fop.flush();
			fop.close();
		}

		if((new File(APP_DIR + LOGFILE + "." + MAX_LOG_NUM)).exists()){
			(new File(APP_DIR + LOGFILE + "." + MAX_LOG_NUM)).delete();
		}

	}
	/**
	 * Move one log line from one file to another file.
	 * @param source File source to split a line
	 * @param dst File destiny to add a line
	 * @param line line needed to move.
	 * @throws IOException
	 */
	void moveLine(File source, File dst, String line) throws IOException{
		int len = line.length();
		String tmpstr = "";

		BufferedReader reader = new BufferedReader(new FileReader(source));
		tmpstr = reader.readLine();
		tmpstr += "\n";
		while(tmpstr.length() < len){
			tmpstr += reader.readLine();
			tmpstr += "\n";
		}
		char[] buf = new char[(int) source.length() - tmpstr.length()];
		reader.read(buf);
		reader.close();

		byte[] data = charToByte(buf);

		writeBinaryData(data, source, false);
		writeBinaryData(line.getBytes(), source, true);

		if(dst.length() + tmpstr.length() >= LOGSIZE){
			moveLine(dst, getNextLog(dst), tmpstr);
		}else{
			data = tmpstr.getBytes();
			writeBinaryData(data, dst, true);
		}
	}
	/**
	 * Get next log to backup
	 * @param file Filename of current log file
	 * @return Filename of next log file
	 */
	private File getNextLog(File file) {
		String filename = file.getAbsolutePath();
		int n;
		File filedst = null;
		try{
			n = Integer.parseInt(filename.substring(filename.lastIndexOf(".") + 1));
			filedst = new File(file.getAbsolutePath().substring(0, 
					filename.lastIndexOf(".") + 1) + (n+1));	
		} catch(NumberFormatException e){
			filedst = new File(file.getAbsolutePath().substring(0, 
					filename.lastIndexOf(".")) + ".log.1");
		}
		return filedst;
	}
	/**
	 * Parse char array to byte array
	 * @param buffer char array to be converted.
	 * @return byte array converted.
	 */
	public byte[] charToByte(char[] buffer){
		byte[] data = new byte[buffer.length];
		for (int j = 0; j < buffer.length; ++j) {
			data[j] = (byte) buffer[j];
		}
		return data;
	}
	/**
	 * Write specified data to specified file.
	 * @param data Data to write on file.
	 * @param file File that to be written.
	 * @param append If is True, it will append the data, otherwise it overwrite all file content.
	 * @throws IOException
	 */
	public void writeBinaryData(byte[] data, File file, Boolean append) throws IOException{
		FileOutputStream fop = new FileOutputStream(file, append);
		fop.write(data);
		fop.flush();
		fop.close();
	}
	/**
	 * Utilities to use shell command.
	 * @author krinekk
	 *
	 */
	public class ShellCommand {
		/**
		 * If it's true, is possible to grant superuser privileges
		 */
		private Boolean can_su;    

		/**
		 * normal SH
		 */
		public SH sh;
		/**
		 * SH with superuser privileges
		 */
		public SH su;

		/**
		 * Constructor by default
		 */
		public ShellCommand() {
			sh = new SH("sh");
			su = new SH("su");
		}
		
		/**
		 * Check if it is possible grant superuser privileges
		 * @return Boolean indicating the possibility of get superuser privileges.
		 */
		public boolean canSU() {
			return canSU(false);
		}
		/**
		 * Check if it is possible grant superuser privileges
		 * @param force_check if its true, forces to check the superuser privileges.
		 * @return Boolean indicating if it can su.
		 */
		public boolean canSU(boolean force_check) {
			if (can_su == null || force_check) {
				CommandResult r = su.runWaitFor("id");
				StringBuilder out = new StringBuilder();

				if (r.stdout != null)
					out.append(r.stdout).append(" ; ");
				if (r.stderr != null)
					out.append(r.stderr);

				Log.v("Rosberg Packaging", "canSU() su[" + r.exit_value + "]: " + out);
				can_su = r.success();
			}
			return can_su;
		}
		/**
		 * Ask for using the normal shell or superuser shell.
		 * @return Boolean indicating what shell will be used.
		 */
		public SH suOrSH() {
			return canSU() ? su : sh;
		}
		
		/**
		 * Class container of the result of call some bash command. 
		 * @author krinekk
		 *
		 */
		public class CommandResult {
			/**
			 * Standard output
			 */
			public final String stdout;
			
			/**
			 * Standard error output
			 */
			public final String stderr;
			
			/**
			 * Value returned by the shell.
			 */
			public final Integer exit_value;

			/**
			 * Constructor specifiing all parameters
			 * @param exit_value_in exit value returned.
			 * @param stdout_in output returned.
			 * @param stderr_in error returned.
			 */
			CommandResult(Integer exit_value_in, String stdout_in, String stderr_in)
			{
				exit_value = exit_value_in;
				stdout = stdout_in;
				stderr = stderr_in;
			}
			
			/**
			 * Constructor without output defined.
			 * @param exit_value_in exit value returned.
			 */
			CommandResult(Integer exit_value_in) {
				this(exit_value_in, null, null);
			}
			
			/**
			 * Check if the launched command runs successfully.
			 * @return true if is successful, otherwise returns false.
			 */
			public boolean success() {
				return exit_value != null && exit_value == 0;
			}
		}

		/**
		 * Normal shell script
		 * @author krinekk
		 *
		 */
		public class SH {
			/**
			 * Shell called.
			 */
			private String SHELL = "sh";

			/**
			 * Constructor by default.
			 * @param SHELL_in Shell used.
			 */
			public SH(String SHELL_in) {
				SHELL = SHELL_in;
			}
			
			/**
			 * Process used to run the specified command.
			 * @param s the command to be called.
			 * @return Command process.
			 */
			public Process run(String s) {
				Process process = null;
				try {
					process = Runtime.getRuntime().exec(SHELL);
					DataOutputStream toProcess = new DataOutputStream(process.getOutputStream());
					toProcess.writeBytes("exec " + s + "\n");
					toProcess.flush();
				} catch(Exception e) {
					Log.e("Packaging", "Exception while trying to run: '" + s + "' " + e.getMessage());
					process = null;
				}
				return process;
			}

			/**
			 * Get the stream obtained of calling some command
			 * @param is input stream used to get the lines.
			 * @return The output of the command called.
			 */
			private String getStreamLines(InputStream is) {
				String out = null;
				StringBuffer buffer = null;
				DataInputStream dis = new DataInputStream(is);

				try {
					if (dis.available() > 0) { 
						buffer = new StringBuffer(dis.readLine());
						while(dis.available() > 0)
							buffer.append("\n").append(dis.readLine());
					}
					dis.close();
				} catch (Exception ex) {
					Log.e("Rosberg Packaging", ex.getMessage());
				}
				if (buffer != null)
					out = buffer.toString();
				return out;
			}

			/**
			 * runs some bash command waiting its result.
			 * @param s command to be called.
			 * @return The command result with the result of the command.
			 */
			public CommandResult runWaitFor(String s) {
				Process process = run(s);
				Integer exit_value = null;
				String stdout = null;
				String stderr = null;
				if (process != null) {
					try {
						exit_value = process.waitFor();

						stdout = getStreamLines(process.getInputStream());
						stderr = getStreamLines(process.getErrorStream());

					} catch(InterruptedException e) {
						Log.e("Rosberg Packaging", "runWaitFor " + e.toString());
					} catch(NullPointerException e) {
						Log.e("Rosberg Packaging", "runWaitFor " + e.toString());
					}
				}
				return new CommandResult(exit_value, stdout, stderr);
			}
		}
	}	

	/**
	 * Returns the stack trace of exception to a String.
	 * @param e the Exception thrown.
	 * @return String containing the stack trace.
	 */
	public String stackTraceToString(Throwable e) {
		StringBuilder sb = new StringBuilder();
		for (StackTraceElement element : e.getStackTrace()) {
			sb.append(element.toString());
			sb.append("\n");
		}
		return sb.toString();
	}
	
	public void logException(Exception e){
		try {
			log("Rosberg Callback", this, 
					"Error " + e.getClass().getName() + ": " + e.getMessage() + " " +
							stackTraceToString(e), Log.ERROR);
		} catch (NameNotFoundException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
	}
}
